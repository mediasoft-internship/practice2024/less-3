package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

type closer interface {
	Close() error
}

type namer interface {
	name() string
}

type namedCloser interface {
	closer
	namer
}

type nameClose struct {
	closer
	Name string
}

func (nc *nameClose) name() string {
	return nc.Name
}

func main() {
	ch := newCloseHub()
	defer ch.CloseAll()

	// OPEN
	file, err := os.OpenFile("004-oop-interface/text.txt", os.O_RDONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	ch.Add("file", file)

	resp, err := http.Get("http://example.com/")
	if err != nil {
		log.Fatal(err)
	}
	ch.Add("response body", resp.Body)

	// READ
	text, err := io.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println((string)(text))

	text, err = io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println((string)(text))

}

type closeHub struct {
	closers []namedCloser
}

func newCloseHub() *closeHub {
	return &closeHub{
		closers: []namedCloser{},
	}
}

func (ch *closeHub) Add(name string, c closer) {
	ch.closers = append(ch.closers, &nameClose{Name: name, closer: c})
}

func (ch *closeHub) CloseAll() {
	for _, c := range ch.closers {
		if err := c.Close(); err != nil {
			log.Printf("Error while closing, name: %s err: %v", c.name(), err)
			continue
		}
		log.Println("close: ", c.name())
	}
}
