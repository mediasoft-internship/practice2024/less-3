package stack

type Stack struct {
	a    []any
	head int
}

func NewStack(size int) *Stack {
	return &Stack{
		a:    make([]any, size),
		head: -1,
	}
}

func (s *Stack) Push(v any) {
	s.head++
	s.a[s.head] = v
}

func (s *Stack) Pop() any {
	if s.head < 0 {
		return nil
	}
	v := s.a[s.head]
	s.head--
	return v
}

func (s *Stack) Peek() any {
	if s.head < 0 {
		return nil
	}
	return s.a[s.head]
}
