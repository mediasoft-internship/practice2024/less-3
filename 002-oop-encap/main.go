package main

import (
	"fmt"
	"gitlab.com/mediasoft-internship/practice2024/less-3/002-oop-encap/stack"
	"log"
)

func main() {
	s := stack.NewStack(100)
	s.Push("10kjdnfj00")
	fmt.Println(s.Pop())
	for i := 0; i < 10; i++ {
		s.Push(i)
	}

	v, ok := s.Pop().(int)
	if !ok {
		log.Fatal("invalid type")
	}
	s.Push(v + 100)

	for i := 10; i > 0; i-- {
		fmt.Println(s.Pop())
	}

	/*	fmt.Println("SIZE 5")
		for i := 0; i < 5; i++ {
			s.Push(i + 20)
		}

		for i := 5; i > 0; i-- {
			fmt.Println(s.Pop())
		}*/
}
