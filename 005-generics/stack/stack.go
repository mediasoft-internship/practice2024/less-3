package stack

type Stacker[T any] interface {
	Push(T)
	Pop() (T, bool)
	Peek() (T, any)
}

type Stack[T any] struct {
	a    []T
	head int
}

func NewStack[T any](size int) *Stack[T] {
	return &Stack[T]{
		a:    make([]T, size),
		head: -1,
	}
}

func (s *Stack[T]) Push(v T) {
	s.head++
	s.a[s.head] = v
}

func (s *Stack[T]) Pop() (T, bool) {
	var v T
	if s.head < 0 {
		return v, false
	}
	v = s.a[s.head]
	s.head--
	return v, true
}

func (s *Stack[T]) Peek() (T, any) {
	if s.head < 0 {
		var v T
		return v, false
	}
	return s.a[s.head], true
}
