package main

import (
	"fmt"
	"gitlab.com/mediasoft-internship/practice2024/less-3/005-generics/stack"
)

func main() {
	sInt := stack.NewStack[int](100)
	for i := 0; i < 10; i++ {
		sInt.Push(i + 200)
	}
	v, _ := sInt.Peek()
	fmt.Println(v + 10)

	sString := stack.NewStack[string](100)
	words := []string{"!!!", "WORLD", "HELLO "}
	message := ""
	for _, w := range words {
		sString.Push(w)
	}

	for {
		s, ok := sString.Pop()
		if !ok {
			break
		}
		message += s
	}

	fmt.Println(message)
}
