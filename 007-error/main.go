package main

import (
	"database/sql"
	"errors"
	"fmt"
)

//type Error interface {
//	Error() string
//}

var errInvalidArgs = errors.New("error invalid arguments")

type SqlErr struct {
	Sql  string
	Err  error
	Args []interface{}
}

func (e SqlErr) Error() string {
	return fmt.Sprintf(
		"error while executing SQL query: %s, args: %v, internal err: %v",
		e.Sql, e.Args, e.Err,
	)
}

func (e SqlErr) Unwrap() error {
	return e.Err
}

func main() {
	// правильное сравнение ошибок error.Is
	sErr := SqlErr{
		Sql:  `select * from employees where ids in ($1)`,
		Err:  sql.ErrNoRows,
		Args: []interface{}{1, 3},
	}

	fmt.Println("sErr, sql.ErrNoRows: ", errors.Is(sErr, sql.ErrNoRows))
	fmt.Println("sErr, SqlErr{}: ", errors.Is(sErr, SqlErr{}))
	fmt.Println("sErr, &SqlErr{}: ", errors.As(sErr, &SqlErr{}))

	argErr := fmt.Errorf(
		"invalid human age: %d, err: %w", 1000, errInvalidArgs)

	fmt.Println("argErr, errInvalidArgs: ", errors.Is(argErr, errInvalidArgs))

	var err error
	err = sErr
	switch err.(type) {
	case SqlErr:
		if errors.Is(sErr, sql.ErrNoRows) {
			fmt.Println("wrap 404 ")
		}
	default:
		fmt.Println("Unknown error")
	}
}
