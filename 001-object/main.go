package main

import "fmt"

type stack struct {
	a    []any
	head int
}

func newStack(size int) *stack {
	return &stack{
		a:    make([]any, size),
		head: -1,
	}
}

func (s *stack) push(v any) {
	s.head++
	s.a[s.head] = v
}

func (s *stack) pop() any {
	if s.head < 0 {
		return nil
	}
	v := s.a[s.head]
	s.head--
	return v
}

func (s *stack) peek() any {
	if s.head < 0 {
		return nil
	}
	return s.a[s.head]
}

func main() {
	s := newStack(100)
	for i := 0; i < 10; i++ {
		s.push(i)
	}

	for i := 10; i > 0; i-- {
		fmt.Println(s.pop())
	}

	fmt.Println("SIZE 5")
	for i := 0; i < 5; i++ {
		s.push(i + 20)
	}

	for i := 5; i > 0; i-- {
		fmt.Println(s.pop())
	}
}
