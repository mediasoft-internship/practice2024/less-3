package main

import "fmt"

type mob struct {
	hp    int
	maxHP int
}

func (m mob) formatHP() string {
	return fmt.Sprintf("%d / %d", m.hp, m.maxHP)
}

type creeper struct {
	mob
}

func (c creeper) formatHP() string {
	return fmt.Sprintf("%d / %d", c.maxHP, c.hp)
}

func (c *creeper) boom() {
	fmt.Println("BOOOOOOM!!!")
	c.hp = 0
}

type skeleton struct {
	mob
}

func (s skeleton) shot() {
	fmt.Println("skeleton fired from a bow")
}

func main() {
	creep := creeper{
		mob{
			hp:    100,
			maxHP: 100,
		},
	}

	skel := skeleton{
		mob{
			hp:    100,
			maxHP: 100,
		},
	}

	fmt.Println("creeper hp: ", creep.formatHP())
	fmt.Println("skeleton hp: ", skel.formatHP())

	creep.boom()
	skel.shot()

	fmt.Println("creeper hp: ", creep.formatHP())
	fmt.Println("skeleton hp: ", skel.formatHP())
}
