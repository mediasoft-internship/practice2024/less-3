package main

import "fmt"

func main() {
	fmt.Println(sum([]float64{0.2, 0.34, 0.004, 00.32}))
	fmt.Println(sum([]int64{2, 34, 4, 32}))

	s := []int{333, 44, 3, 23, 4, 234, 5433, 34}
	insertionSort(s)
	fmt.Println(s)
}

func sum[T int64 | float64](s []T) T {
	var res T
	for _, v := range s {
		res += v
	}
	return res
}

type CustomInt int64

type number interface {
	int32 | ~int64 | float32 | float64 | int
}

func insertionSort[V number](s []V) {
	for i := 0; i < len(s); i++ {
		for j := i; j > 0 && s[j-1] > s[j]; j-- {
			tmp := s[j-1]
			s[j-1] = s[j]
			s[j] = tmp
		}
	}
}
